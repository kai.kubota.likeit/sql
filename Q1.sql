CREATE TABLE item_category (
	category_id int PRIMARY KEY auto_increment,
	category_name varchar(256) not null
);
